# Test Android Application #

Starting point of sample application is MainActivity.java class. It contains:

* Edit text for typing new message
* Button for sending typed message in order to parse it's content
* RecyclerView for showing processed Message object

The test application has two representation modes that can be switched by checking on/off Toolbar action "JsonView"

* JsonView enabled (default) - representation mode according to task specification. It showes up parsed content in format of Json Object. It ignores regular words and delimiters.
* JsonView disabled - it is a more user-friendly representation mode. It creates spannables for each type of lexems inside the message.

**Underlined** - Mentions, for examble @ alex

**Gray Typeface** - Emoticons for example "(cofee)" 

**Blue Typeface** - Links. Alongside with changing font color it replaces url with page title.

Plain words and delimiters are not changed by this mode.

### Architecture considerations ###

The main idea is to have some kind of Unit of Work elements and combine them into more complex operations.
In the application, message parsing operation is divided into two parts and may consist of several such a commands:
First of all ParseMessageCommand divide the message string into lexems. After that we can process some of them and retrieve additional information with separate commands.
Such approach simplifies unit testing of different parts of application logic. However from User's perspective ParseMessageCommand gets simple String as an input parameter and returns end result after all parts of operation has been completed.

### Execution ###

Executor provides functionality of performing simple operation out of the main thread. Since it can't control how often task will be produced, it should avoid multiple execution of the equal tasks at the same time. It uses task cache pattern described in Java Concurrency in Practice book, however it caches tasks for only time of execution in order to keep things simple. It can be extended if neccessary.