package com.example.testhipchat;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;

import com.example.testhipchat.content.Delimiter;
import com.example.testhipchat.content.Link;
import com.example.testhipchat.content.Mention;
import com.example.testhipchat.content.Smiley;
import com.example.testhipchat.content.Word;

/**
 * Created by k.kharkov on 06.06.2016.
 */
class SpannableVisitor extends MessagesAdapter.SpanTextVisitor {
    private final SpannableStringBuilder stringBuilder;
    private final Context context;

    public SpannableVisitor(Context context, SpannableStringBuilder stringBuilder) {
        this.stringBuilder = stringBuilder;
        this.context = context;
    }

    @Override
    public void visit(Link link) {
        appendStringWithSpan(getDisplayLink(link), new ForegroundColorSpan(
                context.getResources().getColor(android.R.color.holo_blue_light)));
    }

    @NonNull
    private String getDisplayLink(Link link) {
        String title = link.getTitle();
        return TextUtils.isEmpty(title) ? link.getUrl() : title;
    }

    private void appendStringWithSpan(String str, Object span) {
        int start = stringBuilder.length();
        int end = start + str.length();
        stringBuilder.append(str);
        stringBuilder.setSpan(span, start, end, Spanned.SPAN_COMPOSING);
    }

    @Override
    public void visit(Mention mention) {
        appendStringWithSpan(mention.getName(), new UnderlineSpan());
    }

    @Override
    public void visit(Smiley smiley) {
        appendStringWithSpan(smiley.getId(), new ForegroundColorSpan(Color.GRAY));
    }

    @Override
    public void visit(Word word) {
        stringBuilder.append(word.toString());
    }

    @Override
    public void visit(Delimiter delimiter) {
        stringBuilder.append(delimiter.toString());
    }

    @Override
    public CharSequence getCharSequence() {
        return stringBuilder.subSequence(0, stringBuilder.length());
    }
}
