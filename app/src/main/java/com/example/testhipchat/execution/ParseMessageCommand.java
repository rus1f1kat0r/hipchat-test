package com.example.testhipchat.execution;

import com.example.testhipchat.content.Link;
import com.example.testhipchat.content.Message;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by k.kharkov on 05.06.2016.
 */
public class ParseMessageCommand extends CommandGroup<String, CommandStatus<Message>> {

    public ParseMessageCommand(String params) {
        super(params);
    }

    @Override
    protected CommandStatus<Message> onExecute() {
        try {
            Message initialInfo = executeCommand(new GetMessageInfo(getParams()));
            Set<Link> unique = new LinkedHashSet<>();
            unique.addAll(initialInfo.getLinks());
            for (Link link : unique) {
                fetchTitleFor(link, initialInfo.getLinks());
            }
            return new CommandStatus.Ok<>(initialInfo);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return new CommandStatus.Error<>(e.getMessage());
        }
    }

    private void fetchTitleFor(Link link, List<Link> links) {
        try {
            CommandStatus<String> result = executeCommand(
                    new FetchTitle(new FetchTitle.Param(link.getUrl())));
            if (CommandStatus.isOk(result)) {
                String title = ((CommandStatus.Ok<String>)result).getValue();
                setTitleForAllEqualLinks(link, links, title);
            }
        } catch (InterruptedException e) {
            // just leave current link without changes
            // we can try to fetch title again later
            e.printStackTrace();
        }

    }

    private void setTitleForAllEqualLinks(Link link, List<Link> links, String title) {
        // optimization consideration:
        // make independent command to run in parallel
        for (Link each : links) {
            if (each.equals(link)) {
                link.setTitle(title);
            }
        }
    }
}
