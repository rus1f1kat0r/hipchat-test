package com.example.testhipchat.execution;

import com.example.testhipchat.content.Message;
import com.example.testhipchat.parser.MessageParserImpl;

/**
 * Created by k.kharkov on 05.06.2016.
 */
public class GetMessageInfo extends Command<String, Message> {

    public GetMessageInfo(String params) {
        super(params);
    }

    @Override
    protected Message onExecute() {
        return new MessageParserImpl().parse(getParams());
    }
}
