package com.example.testhipchat.execution;

/**
 * Created by k.kharkov on 05.06.2016.
 */
public class CommandStatus<V> {

    public static boolean isOk(CommandStatus result) {
        return result != null && Ok.class.isInstance(result);
    }

    public static class Ok<V> extends CommandStatus<V> {
        private final V result;

        public Ok(V result) {
            this.result = result;
        }

        public V getValue() {
            return result;
        }
    }

    public static class Error<V> extends CommandStatus<V> {
        private final String errorMessage;

        public Error(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        @Override
        public String toString() {
            return "Error " + errorMessage;
        }
    }

    public static class UnknownError<V> extends Error<V> {
        public UnknownError(String errorMessage) {
            super(errorMessage);
        }
    }
}
