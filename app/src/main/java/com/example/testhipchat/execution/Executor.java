package com.example.testhipchat.execution;

import java.util.concurrent.Future;

/**
 * Created by k.kharkov on 04.06.2016.
 */
public interface Executor {
    <V> Future<V> execute(Command<?, V> cmd) ;
}
