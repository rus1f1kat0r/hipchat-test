package com.example.testhipchat.execution;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by k.kharkov on 04.06.2016.
 */
public class FetchTitle extends Command<FetchTitle.Param, CommandStatus<String>> {

    private static final String TAG = "FetchTitle";

    public FetchTitle(Param params) {
        super(params);
    }

    @Override
    protected CommandStatus<String> onExecute() {
        try {
            Document doc = getParams().getConnection().get();
            String title = doc.title();
            return new CommandStatus.Ok<>(title);
        } catch (IOException e) {
            return new CommandStatus.Error<>(e.getMessage());
        } catch (SecurityException e) {
            return new CommandStatus.Error<>(e.getMessage());
        } catch (Throwable e) {
            return new CommandStatus.UnknownError<>(e.getMessage());
        }
    }

    public static class Param {
        private final String url;

        public Param(String url) {
            this.url = url;
        }

        public Connection getConnection() {
            return Jsoup.connect(url);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Param param = (Param) o;

            return url != null ? url.equals(param.url) : param.url == null;

        }

        @Override
        public int hashCode() {
            return url != null ? url.hashCode() : 0;
        }
    }
}
