package com.example.testhipchat.execution;

import android.support.annotation.NonNull;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by k.kharkov on 04.06.2016.
 */
public class ExecutorImpl implements Executor {

    private final ConcurrentMap<Command<?, ?>, Future<?>> tasks;
    private final java.util.concurrent.Executor service;

    public ExecutorImpl() {
        this(Executors.newFixedThreadPool(5));
    }

    public ExecutorImpl(java.util.concurrent.Executor service) {
        this.service = service;
        this.tasks = new ConcurrentHashMap<>();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <V> Future<V> execute(final Command<?, V> cmd) {
        Callable<V> callable = new Callable<V>(){

            @Override
            public V call() throws Exception {
                return cmd.execute();
            }
        };
        RunnableFuture<V> result = new RemoveFutureTask<>(cmd, callable);
        RunnableFuture<V> existing = (RunnableFuture<V>) tasks.putIfAbsent(cmd, result);
        if (existing != null) {
            result = existing;
        } else {
            service.execute(result);
        }
        return result;
    }

    private class RemoveFutureTask<V> extends FutureTask<V> {

        private final Command<?, V> command;

        public RemoveFutureTask(Command<?, V> cmd, Callable<V> callable) {
            super(callable);
            this.command = cmd;
        }

        @Override
        protected void done() {
            super.done();
            tasks.remove(command, this);
        }

        @Override
        public V get() throws InterruptedException, ExecutionException {
            try {
                return super.get();
            } finally {
                tasks.remove(command, this);
            }
        }

        @Override
        public V get(long timeout, @NonNull TimeUnit unit) throws InterruptedException,
                ExecutionException, TimeoutException {
            try {
                return super.get(timeout, unit);
            } finally {
                tasks.remove(command, this);
            }
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            try {
                return super.cancel(mayInterruptIfRunning);
            } finally {
                tasks.remove(command, this);
            }
        }
    }
}
