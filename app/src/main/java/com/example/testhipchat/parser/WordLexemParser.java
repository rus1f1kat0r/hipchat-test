package com.example.testhipchat.parser;

import android.support.annotation.NonNull;

import com.example.testhipchat.content.Lexem;
import com.example.testhipchat.content.Word;

/**
 * Created by k.kharkov on 04.06.2016.
 */
class WordLexemParser extends DelimiterLexemParser{

    @Override
    public boolean isStart(char c) {
        return Character.isLetter(c) || Character.isDigit(c) || Character.isDefined(c);
    }

    @NonNull
    @Override
    public MessageParserImpl.LexemParseResult parseLexem(@NonNull String message, int start) {
        int nextToken = getEndIndex(message, start);
        return getWord(message, start, nextToken);
    }

    protected int getEndIndex(String message, int start) {
        int nextToken = start + 1;
        while (nextToken < message.length()) {
            char token = message.charAt(nextToken);
            if (super.isStart(token)) {
                break;
            }
            nextToken ++;
        }
        return nextToken;
    }

    @NonNull
    protected MessageParserImpl.LexemParseResult getWord(String message, int start, int nextToken) {
        Lexem lexem = new Word(message.substring(start, nextToken));
        return new MessageParserImpl.LexemParseResult(lexem, nextToken);
    }

    protected static boolean isAlphanumeric(String name) {
        for (int i = 0; i < name.length(); i ++ ) {
            char current = name.charAt(i);
            if (!Character.isDigit(current) && !Character.isLetter(current)) {
                return false;
            }
        }
        return true;
    }
}
