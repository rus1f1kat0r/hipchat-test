package com.example.testhipchat.parser;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.testhipchat.content.Lexem;
import com.example.testhipchat.content.Link;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by k.kharkov on 04.06.2016.
 */
class LinkLexemParser extends WordLexemParser {

    public static final Pattern URL_PATTERN = Pattern.compile("^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$");

    @Override
    public boolean isStart(char c) {
        return c == 'h'; // for http|https only
    }

    @NonNull
    @Override
    public MessageParserImpl.LexemParseResult parseLexem(@NonNull String message, int start) {
        int nextToken = getNextSpaceSeparatorIndex(message, start);
        String possibleLink = message.substring(start, nextToken);
        Matcher matcher = URL_PATTERN.matcher(possibleLink);
        if (matcher.matches()) {
            Lexem link = new Link(possibleLink, "");
            return new MessageParserImpl.LexemParseResult(link, nextToken);
        } else {
            return super.parseLexem(message, start);
        }
    }

    private int getNextSpaceSeparatorIndex(String message, int start) {
        int nextToken = start + 1;
        while (nextToken < message.length()) {
            char token = message.charAt(nextToken);
            // dots and commas are allowed inside urls
            if (isSpaceSeparator(token)) {
                break;
            }
            nextToken ++;
        }
        return nextToken;
    }
}
