package com.example.testhipchat.parser;

import android.support.annotation.NonNull;

import com.example.testhipchat.content.Delimiter;

/**
 * Created by k.kharkov on 04.06.2016.
 */
class DelimiterLexemParser implements MessageParserImpl.LexemParser {

    @Override
    public boolean isStart(char c) {
        return isDelimiter(c) || isSpaceSeparator(c);
    }

    protected boolean isSpaceSeparator(char c) {
        return c == ' ' || c == '\n' || c == '\r';
    }

    protected boolean isDelimiter(char c) {
        return c == ',' || c == '.';
    }

    @NonNull
    @Override
    public MessageParserImpl.LexemParseResult parseLexem(@NonNull String message, int start) {
        int end = start;
        while (end < message.length()) {
            if (!isStart(message.charAt(end))) {
                break;
            }
            end ++;
        }
        Delimiter lexem = new Delimiter(message.substring(start, end));
        return new MessageParserImpl.LexemParseResult(lexem, end);
    }
}
