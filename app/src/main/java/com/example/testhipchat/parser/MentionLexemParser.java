package com.example.testhipchat.parser;

import android.support.annotation.NonNull;
import android.support.v4.text.TextUtilsCompat;
import android.text.TextUtils;

import com.example.testhipchat.content.Lexem;
import com.example.testhipchat.content.Mention;
import com.example.testhipchat.content.Word;

/**
 * Created by k.kharkov on 04.06.2016.
 */
class MentionLexemParser extends WordLexemParser {

    @Override
    public boolean isStart(char c) {
        return c == '@';
    }

    @NonNull
    @Override
    public MessageParserImpl.LexemParseResult parseLexem(@NonNull String message, int start) {
        int nextToken = getEndIndex(message, start);
        if (nextToken > start + 1) {
            return getMentionIfAlphanumeric(message, start, nextToken);
        } else {
            return getWord(message, start, nextToken);
        }
    }

    @NonNull
    private MessageParserImpl.LexemParseResult getMentionIfAlphanumeric(String message, int start, int nextToken) {
        String name = message.substring(start + 1, nextToken);
        Lexem lexem = isAlphanumeric(name) ? new Mention(name) :
                new Word(message.substring(start, nextToken));
        return new MessageParserImpl.LexemParseResult(lexem, nextToken);
    }
}
