package com.example.testhipchat.parser;

import android.support.annotation.NonNull;

import com.example.testhipchat.content.Lexem;
import com.example.testhipchat.content.Smiley;

/**
 * Created by k.kharkov on 04.06.2016.
 */
class SmileyLexemParser extends WordLexemParser {

    private static final int MIN_SMILEY_LENGTH = 3; // at least one char and two parenthesis
    private static final int MAX_SMILEY_LENGTH = 17;// 15 characters for smiley id and two parenthesis

    @Override
    public boolean isStart(char c) {
        return c == '(';
    }

    @NonNull
    @Override
    public MessageParserImpl.LexemParseResult parseLexem(@NonNull String message, int start) {
        int nextToken = getEndIndex(message, start);
        int length = nextToken - start;
        if (isValidSmiley(message, start, nextToken, length)) {
            return getSmiley(message, start, nextToken);
        } else {
            return getWord(message, start, nextToken);
        }
    }

    private boolean isValidSmiley(@NonNull String message, int start, int nextToken, int length) {
        return  length >= MIN_SMILEY_LENGTH &&
                length <= MAX_SMILEY_LENGTH &&
                message.charAt(nextToken - 1) == ')' &&
                isAlphanumeric(message.substring(start + 1, nextToken - 1));
    }

    @NonNull
    private MessageParserImpl.LexemParseResult getSmiley(String message, int start, int nextToken) {
        Lexem smiley = new Smiley(message.substring(start + 1, nextToken - 1), "");
        return new MessageParserImpl.LexemParseResult(smiley, nextToken);
    }
}
