package com.example.testhipchat.parser;

import android.support.annotation.NonNull;

import com.example.testhipchat.content.Message;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public interface MessageParser {
    @NonNull
    Message parse(String message);
}