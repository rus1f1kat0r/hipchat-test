package com.example.testhipchat.parser;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.testhipchat.content.Delimiter;
import com.example.testhipchat.content.Lexem;
import com.example.testhipchat.content.Link;
import com.example.testhipchat.content.Mention;
import com.example.testhipchat.content.Message;
import com.example.testhipchat.content.Smiley;
import com.example.testhipchat.content.Visitor;
import com.example.testhipchat.content.Word;

import java.util.LinkedList;
import java.util.List;

public class MessageParserImpl implements MessageParser {

    @Override
    @NonNull
    public Message parse(String message) {
        List<Lexem> lexems = parseLexems(message);
        Message.Builder builder = new Message.Builder();
        Visitor visitor = new MessageBuilderVisitor(builder);
        for (Lexem lexem : lexems) {
            lexem.accept(visitor);
        }
        return builder.build();
    }

    @NonNull
    private List<Lexem> parseLexems(String message) {
        List<Lexem> lexems = new LinkedList<>();
        int i = 0;
        while (i < message.length()) {
            char token = message.charAt(i);
            LexemParseResult result = getNextLexem(message, i, token);
            if (result != null) {
                lexems.add(result.lexem);
                i = result.endIndex;
            } else {
                i ++;
            }
        }
        return lexems;
    }

    @Nullable
    private LexemParseResult getNextLexem(String message, int i, char token) {
        for (Lex start : Lex.values()) {
            if (start.parser.isStart(token)) {
                return start.parser.parseLexem(message, i);
            }
        }
        return null;
    }

    static class LexemParseResult{
        private final Lexem lexem;
        private final int endIndex;

        public LexemParseResult(Lexem lexem, int endIndex) {
            this.lexem = lexem;
            this.endIndex = endIndex;
        }
    }

    private enum Lex{
        MENTION(new MentionLexemParser()),
        DELIMITER(new DelimiterLexemParser()),
        SMILEY(new SmileyLexemParser()),
        LINK(new LinkLexemParser()),
        WORD(new WordLexemParser());

        private final LexemParser parser;

        Lex(LexemParser parser) {
            this.parser = parser;
        }

    }

    private class MessageBuilderVisitor implements Visitor {

        @NonNull
        private final Message.Builder builder;

        private MessageBuilderVisitor(@NonNull Message.Builder builder) {
            this.builder = builder;
        }

        @Override
        public void visit(Link link) {
            builder.addLink(link);
        }

        @Override
        public void visit(Mention mention) {
            builder.addMention(mention);
        }

        @Override
        public void visit(Smiley smiley) {
            builder.addSmiley(smiley);
        }

        @Override
        public void visit(Word word) {
            builder.addWord(word);
        }

        @Override
        public void visit(Delimiter delimiter) {
            builder.addDelimiter(delimiter);
        }
    }

    public interface LexemParser {
        boolean isStart(char c);
        @Nullable
        LexemParseResult parseLexem(@NonNull String message, int start);
    }
}
