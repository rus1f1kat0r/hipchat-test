package com.example.testhipchat;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.testhipchat.content.Message;
import com.example.testhipchat.execution.Command;
import com.example.testhipchat.execution.CommandStatus;
import com.example.testhipchat.execution.ParseMessageCommand;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.prefs.Preferences;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_JSON_VIEW = "JSON_VIEW";

    private EditText editText;
    private ImageButton send;
    private RecyclerView messages;
    private MessagesAdapter adapter;

    private final View.OnClickListener sendListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String message = editText.getText().toString();
            if (!TextUtils.isEmpty(message)) {
                sendMessage(message);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.editText);
        send = (ImageButton) findViewById(R.id.imageButton);
        send.setOnClickListener(sendListener);
        messages = (RecyclerView) findViewById(R.id.recycler);
        setUpAdapter();
    }

    private void setUpAdapter() {
        messages.setHasFixedSize(true);
        messages.setItemAnimator(new DefaultItemAnimator());
        messages.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MessagesAdapter(this);
        messages.setAdapter(adapter);
    }

    private void sendMessage(String message) {
        try {
            new ParseMessageAsyncTask().execute(message);
        } catch (RejectedExecutionException e) {
            Toast.makeText(this, R.string.too_many_tasks, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.present_switch, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        menu.findItem(R.id.switcher).setChecked(prefs.getBoolean(KEY_JSON_VIEW, true));
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.switcher) {
            PreferenceManager.getDefaultSharedPreferences(this)
                    .edit()
                    .putBoolean(KEY_JSON_VIEW, !item.isChecked())
                    .apply();
            invalidateOptionsMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    private class ParseMessageAsyncTask extends AsyncTask<String, Void, CommandStatus<Message>> {

        @Override
        protected CommandStatus<Message> doInBackground(String... params) {
            return new ParseMessageCommand(params[0]).execute();
        }

        @Override
        protected void onPostExecute(CommandStatus<Message> messageCommandStatus) {
            super.onPostExecute(messageCommandStatus);
            if (CommandStatus.isOk(messageCommandStatus)) {
                adapter.addMessage(((CommandStatus.Ok<Message>)messageCommandStatus).getValue());
            } else {
                Toast.makeText(
                        MainActivity.this,
                        R.string.error_parsing_message,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
