package com.example.testhipchat.content;

import android.support.annotation.NonNull;

/**
 * Created by k.kharkov on 04.06.2016.
 */
public class Delimiter implements Lexem{

    @NonNull
    private final String delimiter;

    public Delimiter(@NonNull String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Delimiter delimiter1 = (Delimiter) o;

        return delimiter.equals(delimiter1.delimiter);
    }

    @Override
    public int hashCode() {
        return delimiter.hashCode();
    }

    @Override
    public String toString() {
        return delimiter;
    }
}
