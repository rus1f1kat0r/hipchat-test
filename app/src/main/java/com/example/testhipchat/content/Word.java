package com.example.testhipchat.content;

import android.support.annotation.NonNull;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public class Word implements Lexem {

    @NonNull
    private final String word;


    public Word(@NonNull String word) {
        this.word = word;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word1 = (Word) o;

        return word.equals(word1.word);

    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

    @Override
    public String toString() {
        return word + "";
    }
}
