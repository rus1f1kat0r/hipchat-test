package com.example.testhipchat.content;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public class Message {
    @NonNull
    private final List<Mention> mentions;
    @NonNull
    private final List<Link> links;
    @NonNull
    private final List<Smiley> smileys;
    @NonNull
    private final List<Word> words;
    @NonNull
    private final List<Delimiter> delimiters;
    @NonNull
    private final List<Lexem> all;

    private Message(Builder builder) {
        // avoid collection mutation via builder reference
        this.mentions = copyList(builder.mentions);
        this.links = copyList(builder.links);
        this.smileys = copyList(builder.smileys);
        this.words = copyList(builder.words);
        this.delimiters = copyList(builder.delimiters);
        this.all = copyList(builder.all);
    }

    private static <T> List<T> copyList(List<T> from) {
        return new LinkedList<>(from);
    }

    @NonNull
    public List<Mention> getMentions() {
        return Collections.unmodifiableList(mentions);
    }

    @NonNull
    public List<Smiley> getSmileys() {
        return Collections.unmodifiableList(smileys);
    }

    @NonNull
    public List<Link> getLinks() {
        return Collections.unmodifiableList(links);
    }

    @NonNull
    public List<Word> getWords() {
        return Collections.unmodifiableList(words);
    }

    @NonNull
    public List<Delimiter> getDelimiters() {
        return Collections.unmodifiableList(delimiters);
    }

    @NonNull
    public List<Lexem> getAll() {
        return Collections.unmodifiableList(all);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Lexem lexem : all) {
            builder.append(lexem.toString());
        }
        return builder.toString();
    }

    public static class Builder {
        @NonNull
        private final List<Mention> mentions;
        @NonNull
        private final List<Link> links;
        @NonNull
        private final List<Smiley> smileys;
        @NonNull
        private final List<Word> words;
        @NonNull
        private final List<Delimiter> delimiters;
        @NonNull
        private final List<Lexem> all;

        public Builder() {
            this.mentions = new LinkedList<>();
            this.links = new LinkedList<>();
            this.smileys = new LinkedList<>();
            this.words = new LinkedList<>();
            this.delimiters = new LinkedList<>();
            this.all = new LinkedList<>();
        }

        public Builder addMention(@NonNull Mention mention) {
            this.mentions.add(mention);
            this.all.add(mention);
            return this;
        }

        public Builder addLink(@NonNull  Link link) {
            this.links.add(link);
            this.all.add(link);
            return this;
        }

        public Builder addSmiley(@NonNull Smiley smiley) {
            this.smileys.add(smiley);
            this.all.add(smiley);
            return this;
        }

        public Builder addWord(@NonNull Word word) {
            this.words.add(word);
            this.all.add(word);
            return this;
        }

        public Builder addDelimiter(@NonNull Delimiter delimiter) {
            this.delimiters.add(delimiter);
            this.all.add(delimiter);
            return this;
        }

        public Message build() {
            return new Message(this);
        }
    }
}
