package com.example.testhipchat.content;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public interface Lexem {
    void accept(Visitor visitor);
}
