package com.example.testhipchat.content;

import android.support.annotation.NonNull;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public class Link implements Lexem{
    @NonNull
    private final String url;
    @NonNull
    private String title;

    public Link(@NonNull String url, @NonNull String title) {
        this.url = url;
        this.title = title;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @NonNull
    public String getUrl() {
        return url;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        if (!url.equals(link.url)) return false;
        return title.equals(link.title);
    }

    @Override
    public int hashCode() {
        int result = url.hashCode();
        result = 31 * result + title.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return url;
    }
}
