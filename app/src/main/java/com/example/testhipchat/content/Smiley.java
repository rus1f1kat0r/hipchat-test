package com.example.testhipchat.content;

import android.support.annotation.NonNull;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public class Smiley implements Lexem{
    @NonNull
    private final String id;
    @NonNull
    private final String imageUrl;

    public Smiley(@NonNull String id, @NonNull String imageUrl) {
        this.id = id;
        this.imageUrl = imageUrl;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Smiley smiley = (Smiley) o;

        if (!id.equals(smiley.id)) return false;
        return imageUrl.equals(smiley.imageUrl);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + imageUrl.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return '(' + id + ')';
    }
}
