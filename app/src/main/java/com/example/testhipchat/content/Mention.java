package com.example.testhipchat.content;

import android.support.annotation.NonNull;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public class Mention implements Lexem{

    @NonNull
    private final String name;

    public Mention(@NonNull String name) {
        this.name = name;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Mention mention = (Mention) o;

        return name != null ? name.equals(mention.name) : mention.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return '@' + name;
    }
}
