package com.example.testhipchat.content;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public interface Visitor {
    void visit(Link link);
    void visit(Mention mention);
    void visit(Smiley smiley);
    void visit(Word word);
    void visit(Delimiter delimiter);
}
