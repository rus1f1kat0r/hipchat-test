package com.example.testhipchat;

import com.example.testhipchat.content.Delimiter;
import com.example.testhipchat.content.Link;
import com.example.testhipchat.content.Mention;
import com.example.testhipchat.content.Smiley;
import com.example.testhipchat.content.Word;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by k.kharkov on 06.06.2016.
 */
class JsonVisitor extends MessagesAdapter.SpanTextVisitor {
    private final JSONArray links = new JSONArray();
    private final JSONArray mentions = new JSONArray();
    private final JSONArray smileys = new JSONArray();

    public JsonVisitor() {
    }

    @Override
    public void visit(Link link) {
        try {
            JSONObject linkObject = new JSONObject();
            linkObject.put("url", link.getUrl());
            linkObject.put("title", link.getTitle());
            links.put(linkObject);
        } catch (JSONException e) {
            //should not happen
            throw new RuntimeException(e);
        }
    }

    @Override
    public void visit(Mention mention) {
        mentions.put(mention.getName());
    }

    @Override
    public void visit(Smiley smiley) {
        smileys.put(smiley.getId());
    }

    @Override
    public void visit(Word word) {
        // skip
    }

    @Override
    public void visit(Delimiter delimiter) {
        // skip
    }

    @Override
    public CharSequence getCharSequence() {
        try {
            JSONObject msg = new JSONObject();
            putIfNotEmpty(mentions, "mentions", msg);
            putIfNotEmpty(smileys, "emoticons", msg);
            putIfNotEmpty(links, "links", msg);
            return msg.toString();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void putIfNotEmpty(JSONArray array, String key, JSONObject msg) throws JSONException {
        if (array.length() > 0) {
            msg.put(key, array);
        }
    }
}
