package com.example.testhipchat;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.testhipchat.content.Lexem;
import com.example.testhipchat.content.Message;
import com.example.testhipchat.content.Visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by k.kharkov on 05.06.2016.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageHolder> {

    private final List<Message> messages = new ArrayList<>();
    private final Context context;
    private final SharedPreferences preferences;
    private final PreferenceViewListener preferenceListener;
    private ViewType viewType;

    public MessagesAdapter(Context context) {
        setHasStableIds(true);
        this.context = context;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.preferenceListener = new PreferenceViewListener();
        this.preferences.registerOnSharedPreferenceChangeListener(preferenceListener);
        updateMessageFormatVisitor(isJsonView());
    }

    private boolean isJsonView() {
        return preferences.getBoolean(MainActivity.KEY_JSON_VIEW, true);
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MessageHolder(LayoutInflater.from(context)
                .inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        final SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        SpanTextVisitor spannableVisitor = viewType.createTextFormatVisitor(context, stringBuilder);
        for (Lexem lexem : messages.get(position).getAll()) {
            lexem.accept(spannableVisitor);
        }
        holder.text.setText(spannableVisitor.getCharSequence());
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void updateMessageFormatVisitor(boolean json) {
        viewType = json ? ViewType.JSON : ViewType.SPAN;
        notifyDataSetChanged();
    }

    public void addMessage(Message message) {
        messages.add(0, message);
        notifyDataSetChanged();
    }

    public static class MessageHolder extends RecyclerView.ViewHolder {

        private final TextView text;

        public MessageHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.message);
        }
    }

    private enum ViewType {
        JSON {
            @Override
            SpanTextVisitor createTextFormatVisitor(Context context, SpannableStringBuilder builder) {
                return new JsonVisitor();
            }
        },
        SPAN {
            @Override
            SpanTextVisitor createTextFormatVisitor(Context context, SpannableStringBuilder builder) {
                return new SpannableVisitor(context, builder);
            }
        };

        abstract SpanTextVisitor createTextFormatVisitor(Context context, SpannableStringBuilder builder);
    }

    abstract static class SpanTextVisitor implements Visitor {
        public abstract CharSequence getCharSequence();
    }

    private class PreferenceViewListener implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (MainActivity.KEY_JSON_VIEW.equals(key)) {
                updateMessageFormatVisitor(sharedPreferences.getBoolean(key, true));
            }
        }
    }
}
