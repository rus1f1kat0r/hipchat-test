package com.example.testhipchat.execution;

import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.stubbing.answers.AnswerReturnValuesAdapter;
import org.mockito.internal.verification.Times;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by k.kharkov on 05.06.2016.
 */
public class FetchTitleTest {

    public static final java.util.concurrent.Executor IMMEDIATE_EXECUTOR = new java.util.concurrent.Executor() {
        @Override
        public void execute(Runnable command) {
            command.run();
        }
    };

    Executor executor;
    FetchTitle.Param param;
    Connection connection;
    Document document;
    Command<FetchTitle.Param, CommandStatus<String>> cmd;

    @Before
    public void setUp() throws Exception {
        executor = new ExecutorImpl(IMMEDIATE_EXECUTOR);
        param = mock(FetchTitle.Param.class);
        connection = mock(Connection.class);
        document = mock(Document.class);
        when(param.getConnection()).thenReturn(connection);
        cmd = new FetchTitle(param);
    }

    @Test
    public void testResultOk() throws Exception {
        final String expectedTitle = "Mail.Ru mail app";
        when(connection.get()).thenReturn(document);
        when(document.title()).thenReturn(expectedTitle);
        CommandStatus<String> result = executor.execute(cmd).get();
        assertThat(result, instanceOf(CommandStatus.Ok.class));
        assertThat(((CommandStatus.Ok<String>) result).getValue(), is(expectedTitle));
    }

    @Test
    public void testIOExceptionWhileGettingUrl() throws Exception {
        String expectedError = "not available";
        when(connection.get()).thenThrow(new IOException(expectedError));
        CommandStatus<String> result = executor.execute(cmd).get();
        assertThat(result, instanceOf(CommandStatus.Error.class));
        assertThat(((CommandStatus.Error<String>)result).getErrorMessage(), is(expectedError));
    }

    @Test
    public void testSecurityExceptionWhileGettingUrl() throws Exception {
        String expectedError = "no internet permission";
        when(connection.get()).thenThrow(new SecurityException(expectedError));
        CommandStatus<String> result = executor.execute(cmd).get();
        assertThat(result, instanceOf(CommandStatus.Error.class));
        assertThat(((CommandStatus.Error<String>)result).getErrorMessage(), is(expectedError));
    }

    @Test
    public void testUnknownException() throws Exception {
        String expectedError = "unknown";
        when(connection.get()).thenReturn(document);
        when(document.title()).thenThrow(new RuntimeException(expectedError));
        CommandStatus<String> result = executor.execute(cmd).get();
        assertThat(result, instanceOf(CommandStatus.UnknownError.class));
        assertThat(((CommandStatus.Error<String>)result).getErrorMessage(), is(expectedError));
    }

    @Test
    public void testCancelBeforeExecution() throws Exception {
        final String expectedTitle = "Mail.Ru mail app";
        when(connection.get()).thenReturn(document);
        when(document.title()).thenReturn(expectedTitle);
        cmd.cancel();
        CommandStatus<String> result = executor.execute(cmd).get();
        assertThat(result, is(nullValue()));
        assertThat(cmd.isCancelled(), is(true));
    }
}