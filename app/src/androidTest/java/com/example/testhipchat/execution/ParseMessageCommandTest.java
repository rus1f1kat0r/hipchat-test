package com.example.testhipchat.execution;

import android.support.annotation.NonNull;

import com.example.testhipchat.content.Delimiter;
import com.example.testhipchat.content.Link;
import com.example.testhipchat.content.Mention;
import com.example.testhipchat.content.Message;
import com.example.testhipchat.content.Word;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.example.testhipchat.execution.ParseMessageCommandTest.CompletedFuture.result;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

/**
 * Created by k.kharkov on 05.06.2016.
 */
public class ParseMessageCommandTest {

    public static final String GOOGLE_LINK = "http://google.com";
    public static final String ATLASSIAN_LINK = "https://atlassian.com";
    public static final String TWO_LINKS = GOOGLE_LINK + " " + ATLASSIAN_LINK;
    Executor mockExecutor;

    @Before
    public void setUp() throws Exception {
        mockExecutor = mock(Executor.class);
        CommandGroup.setDefaultExecutor(mockExecutor);
    }

    @Test
    public void testOk() throws Exception {
        String msg = "@alex, hello";
        Message expectedResult = new Message.Builder()
                .addMention(new Mention("alex"))
                .addDelimiter(new Delimiter(", "))
                .addWord(new Word("hello"))
                .build();
        when(mockExecutor.execute(new GetMessageInfo(msg))).thenReturn(result(expectedResult));
        verifyNoMoreInteractions(mockExecutor);
        ParseMessageCommand cmd = new ParseMessageCommand(msg);
        CommandStatus<Message> result = cmd.execute();
        assertThat(result, instanceOf(CommandStatus.Ok.class));
        assertThat(((CommandStatus.Ok<Message>)result).getValue(), sameInstance(expectedResult));
    }

    @Test
    public void testOkWithLinkFetch() throws Exception {
        Message expectedResult = getMessageForTwoLinks();
        when(mockExecutor.execute(new GetMessageInfo(TWO_LINKS))).thenReturn(result(expectedResult));
        when(mockExecutor.execute(new FetchTitle(new FetchTitle.Param(GOOGLE_LINK))))
                .thenReturn(okTitle("Google"));
        when(mockExecutor.execute(new FetchTitle(new FetchTitle.Param(ATLASSIAN_LINK))))
                .thenReturn(okTitle("Atlassian"));
        verifyNoMoreInteractions(mockExecutor);
        ParseMessageCommand cmd = new ParseMessageCommand(TWO_LINKS);
        Message value = getMessageResultAndAssertOk(expectedResult, cmd);
        assertThat(value.getLinks().get(0), is(new Link(GOOGLE_LINK, "Google")));
        assertThat(value.getLinks().get(1), is(new Link(ATLASSIAN_LINK, "Atlassian")));
    }

    private Message getMessageResultAndAssertOk(Message expectedResult, ParseMessageCommand cmd) {
        CommandStatus<Message> result = cmd.execute();
        assertThat(result, instanceOf(CommandStatus.Ok.class));
        assertThat(result, notNullValue());
        Message value = ((CommandStatus.Ok<Message>) result).getValue();
        assertThat(value, sameInstance(expectedResult));
        return value;
    }

    @Test
    public void testOkButWithErrorFetchingOneTitle() throws Exception {
        Message expectedResult = getMessageForTwoLinks();
        when(mockExecutor.execute(new GetMessageInfo(TWO_LINKS))).thenReturn(result(expectedResult));
        when(mockExecutor.execute(new FetchTitle(new FetchTitle.Param(GOOGLE_LINK))))
                .thenReturn(okTitle("Google"));
        when(mockExecutor.execute(new FetchTitle(new FetchTitle.Param(ATLASSIAN_LINK))))
                .thenReturn(errorTitle());
        verifyNoMoreInteractions(mockExecutor);
        ParseMessageCommand cmd = new ParseMessageCommand(TWO_LINKS);
        Message value = getMessageResultAndAssertOk(expectedResult, cmd);
        assertThat(value.getLinks().get(0), is(new Link(GOOGLE_LINK, "Google")));
        assertThat(value.getLinks().get(1), is(new Link(ATLASSIAN_LINK, "")));
    }

    @Test
    public void testCancelAfterParsingInfo() throws Exception {
        Message expectedResult = getMessageForTwoLinks();
        when(mockExecutor.execute(new GetMessageInfo(TWO_LINKS))).thenReturn(result(expectedResult));
        verifyNoMoreInteractions(mockExecutor);
        ParseMessageCommand cmd = mock(ParseMessageCommand.class, withSettings()
                        .defaultAnswer(CALLS_REAL_METHODS)
                        .spiedInstance(new ParseMessageCommand(TWO_LINKS)));
        when(cmd.isCancelled()).thenReturn(false, false, true);
        verifyNoMoreInteractions(mockExecutor);
        Message value = getMessageResultAndAssertOk(expectedResult, cmd);
        assertThat(value.getLinks().get(0), is(new Link(GOOGLE_LINK, "")));
        assertThat(value.getLinks().get(1), is(new Link(ATLASSIAN_LINK, "")));
        /**
         * all following command skipped but for each the command groups
         * tries to check whether it cancelled or not
         */
        verify(cmd, times(4)).isCancelled();
    }

    @Test
    public void testErrorOnParsingInfo() throws Exception {
        Future<Message> resultFuture = mock(Future.class);
        when(resultFuture.get()).thenThrow(new ExecutionException("error!", null));
        when(mockExecutor.execute(new GetMessageInfo(TWO_LINKS))).thenReturn(resultFuture);
        verifyNoMoreInteractions(mockExecutor);
        ParseMessageCommand cmd = new ParseMessageCommand(TWO_LINKS);
        CommandStatus<Message> result = cmd.execute();
        assertThat(result, instanceOf(CommandStatus.Error.class));
        assertThat(result, notNullValue());
    }

    private static Message getMessageForTwoLinks() {
        return new Message.Builder()
                    .addLink(new Link(GOOGLE_LINK, ""))
                    .addDelimiter(new Delimiter(" "))
                    .addLink(new Link(ATLASSIAN_LINK, ""))
                    .build();
    }

    @NonNull
    private static CompletedFuture<CommandStatus<String>> okTitle(String title) {
        return CompletedFuture.<CommandStatus<String>>result(new CommandStatus.Ok<>(title));
    }

    @NonNull
    private static CompletedFuture<CommandStatus<String>> errorTitle() {
        return CompletedFuture.<CommandStatus<String>>result(
                new CommandStatus.Error<String>("Error"));
    }

    static class CompletedFuture<V> implements Future<V> {
        private final V result;
        private final Throwable throwable;

        static <V> CompletedFuture<V> result(V result) {
            return new CompletedFuture<>(result, null);
        }

        private CompletedFuture(V result, Throwable throwable) {
            this.result = result;
            this.throwable = throwable;
        }

        @Override
        public V get() throws ExecutionException {
            if(throwable != null) {
                throw new ExecutionException(throwable);
            } else {
                return result;
            }
        }

        @Override
        public V get(long timeout, TimeUnit timeUnit) throws ExecutionException {
            if(timeUnit == null) {
                throw new NullPointerException();
            } else if(throwable != null) {
                throw new ExecutionException(throwable);
            } else {
                return this.result;
            }
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return true;
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }
    }
}
