package com.example.testhipchat.execution;

import com.android.internal.util.Predicate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsSame.sameInstance;

/**
 * Created by k.kharkov on 04.06.2016.
 */
public class ExecutorTest {

    Executor executor;
    private int counter = 0;

    @Before
    public void init() {
        executor = new ExecutorImpl(new MyThreadPoolExecutor());
    }
    @After
    public void tearDown() {
        waitForExecutorIdle();
    }

    @Test
    public void testNotNull() throws Exception {
        assertThat(executor.execute(new MockCommand("testParams")), notNullValue());
    }

    @Test
    public void testEmptyAfterExecution() throws Exception {
        Future<Integer> future = executor.execute(new MockCommand("testParams"));
        int result = future.get();
        assertThat(result, is("testParams".hashCode()));
    }

    @Test
    public void testDifferentFuturesForDifferentCommands() throws Exception {
        /**
         * in order to block start of execution and avoid adding second command when the
         * first one has already been executed
         */
        doOnExecutorIdle(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                Future<Integer> f1 = executor.execute(new MockCommand("testParams"));
                Future<Integer> f2 = executor.execute(new MockCommand("tttt"));
                assertThat(f1, not(sameInstance(f2)));
                return null;
            }
        });
    }

    @Test
    public void testSameFutureForSameCommands() throws Exception {
        final MockCommand c1 = new MockCommand("testParams");
        final MockCommand c2 = new MockCommand("testParams");
        List<Future<Integer>> toWait = doOnExecutorIdle(new Callable<List<Future<Integer>>>() {
            @Override
            public List<Future<Integer>> call() throws Exception {
                Future<Integer> f1 = executor.execute(c1);
                Future<Integer> f2 = executor.execute(c2);
                assertThat(f1, sameInstance(f2));
                return Arrays.asList(f1, f2);
            }
        });
        for (Future<Integer> future : toWait) {
            future.get();
        }
        waitForExecutorIdle();
        assertThat(c1.executed, is(true));
        assertThat(c2.executed, is(false));
    }

    @Test
    public void testNewFutureAfterIdle() throws Exception {
        Callable<Future<Integer>> callableForAddFuture = new Callable<Future<Integer>>() {
            @Override
            public Future<Integer> call() throws Exception {
                return executor.execute(new MockCommand("test"));
            }
        };
        Future<Integer> f1 = doOnExecutorIdle(callableForAddFuture);
        int result1 = f1.get();
        waitForExecutorIdle();
        Future<Integer> f2 = doOnExecutorIdle(callableForAddFuture);
        int result2 = f2.get();
        assertThat(result1, is(result2));
        assertThat(f1, not(sameInstance(f2)));
    }

    private void waitForExecutorIdle() {
        doOnExecutorIdle(null);
    }

    private <T> T doOnExecutorIdle(Callable<T> callable) {
        return doOnExecutorCondition(new Predicate<Integer>() {
            @Override
            public boolean apply(Integer integer) {
                return integer.equals(0);
            }
        }, callable);
    }

    private<T> T doOnExecutorCondition(Predicate<Integer> predicate, Callable<T> callable) {
        try {
            synchronized (this) {
                while (!predicate.apply(counter)) {
                    this.wait();
                }
                if (callable != null) {
                    return callable.call();
                }
            }
        } catch (Exception e) {
            fail();
        }
        return null;
    }


    static class MockCommand extends Command<String, Integer> {

        private volatile boolean executed;

        public MockCommand(String params) {
            super(params);
        }

        @Override
        protected Integer onExecute() {
            executed = true;
            return getParams().hashCode();
        }
    }

    private class MyThreadPoolExecutor extends ThreadPoolExecutor {
        public MyThreadPoolExecutor() {
            super(5, 5, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        }

        @Override
        protected void beforeExecute(Thread t, Runnable r) {
            super.beforeExecute(t, r);
            synchronized (ExecutorTest.this) {
                counter ++;
                ExecutorTest.this.notifyAll();
            }
        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            super.afterExecute(r, t);
            synchronized (ExecutorTest.this) {
                counter --;
                ExecutorTest.this.notifyAll();
            }
        }
    }
}
