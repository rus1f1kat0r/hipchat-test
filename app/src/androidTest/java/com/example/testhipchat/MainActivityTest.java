package com.example.testhipchat;

import android.support.annotation.NonNull;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by k.kharkov on 06.06.2016.
 */
@RunWith(JUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void testEditTextAndButtonExist() throws Exception {
        onView(editText()).check(matches(isDisplayed()));
        onView(sendButton()).check(matches(isDisplayed()));
    }

    @Test
    public void testSendMessageWithMention() throws Exception {
        checkMessageInput("@alex, check this", "{\"mentions\":[\"alex\"]}");
    }

    private void checkMessageInput(String input, String itemText) {
        onView(editText()).perform(typeText(input));
        onView(sendButton()).perform(click());
        onView(withId(R.id.recycler))
                .check(matches(atPosition(0, hasDescendant(withText(itemText)))));
    }

    @Test
    public void testMention() throws Exception {
        checkMessageInput("@chris you around?", "{\"mentions\":[\"chris\"]}");
    }

    @Test
    public void testEmoticons() throws Exception {
        checkMessageInput("Good morning! (megusta) (coffee)",
                "{\"emoticons\":[\"megusta\",\"coffee\"]}");
    }

    @Test
    public void testLink() throws Exception {
        checkMessageInput("Olympics are starting soon; http://www.nbcolympics.com",
                "{\"links\":[{\"url\":\"http:\\/\\/www.nbcolympics.com\",\"title\":\"2016 Rio Olympic Games | NBC Olympics\"}]}");
    }

    @Test
    public void testMentionsAndLinks() throws Exception {
        checkMessageInput("@bob @john (success) such a cool feature; https://google.com",
                "{\"mentions\":[\"bob\",\"john\"],\"emoticons\":[\"success\"],\"links\":[{\"url\":\"https:\\/\\/google.com\",\"title\":\"Google\"}]}");
    }

    @NonNull
    private static Matcher<View> sendButton() {
        return withId(R.id.imageButton);
    }

    @NonNull
    private static Matcher<View> editText() {
        return withId(R.id.editText);
    }

    public static Matcher<View> atPosition(final int position, @NonNull final Matcher<View> itemMatcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
                if (viewHolder == null) {
                    // has no item on such position
                    return false;
                }
                return itemMatcher.matches(viewHolder.itemView);
            }
        };
    }
}
