package com.example.testhipchat.parser;

import com.example.testhipchat.content.Delimiter;
import com.example.testhipchat.content.Link;
import com.example.testhipchat.content.Mention;
import com.example.testhipchat.content.Message;
import com.example.testhipchat.content.Smiley;
import com.example.testhipchat.content.Word;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by k.kharkov on 03.06.2016.
 */
public class MessageParserTest {

    private MessageParser parser;

    @Before
    public void setUp() throws Exception {
        parser = new MessageParserImpl();
    }

    @Test
    public void testNonNull() throws Exception {
        assertThat(parser.parse("sample"), is(notNullValue()));
    }

    @Test
    public void testMentionsParsed() throws Exception {
        Message message = parser.parse("@Name hi");
        List<Mention> all = message.getMentions();
        assertThat(all, is(notNullValue()));
        assertThat(all.size(), is(1));
        Mention expected = new Mention("Name");
        assertThat(all.contains(expected), is(true));
    }

    @Test
    public void testMultipleMentions() throws Exception {
        testThreeMentions("aa @Name hi @Name2 sssss @Name3");
    }

    private void testThreeMentions(String msg) {
        Message message = parser.parse(msg);
        List<Mention> all = message.getMentions();
        assertThat(all, is(notNullValue()));
        assertThat(all.size(), is(3));
        assertThat(all, equalTo(Arrays.asList(
                new Mention("Name"),
                new Mention("Name2"),
                new Mention("Name3")
        )));
    }

    @Test
    public void testMentionSeparatedWithDotOrComma() throws Exception {
        testThreeMentions("@Name,@Name2.@Name3");
    }

    @Test
    public void testNotMention() throws Exception {
        Message message = parser.parse("asdfasd@Name sss");
        assertThat(message.getMentions().isEmpty(), is(true));
    }

    @Test
    public void testSmiley() throws Exception {
        Message message = parser.parse("(custom)");
        List<Smiley> smileys = message.getSmileys();
        assertThat(smileys.size(), is(1));
        assertThat(smileys.contains(new Smiley("custom", "")), is(true));
    }

    @Test
    public void testLongSmiley() throws Exception {
        Message message = parser.parse("(custom789012345)");
        List<Smiley> smileys = message.getSmileys();
        assertThat(smileys.size(), is(1));
        assertThat(smileys.contains(new Smiley("custom789012345", "")), is(true));
    }

    @Test
    public void testTooLongSmiley() throws Exception {
        Message message = parser.parse("(custom7890123456)");
        List<Smiley> smileys = message.getSmileys();
        assertThat(smileys.isEmpty(), is(true));
    }

    @Test
    public void testWrongCloseParanthesis() throws Exception {
        Message message = parser.parse("(custom)a");
        List<Smiley> smileys = message.getSmileys();
        assertThat(smileys.isEmpty(), is(true));
    }

    @Test
    public void testWrongStartOfSmiley() throws Exception {
        Message message = parser.parse("a(custom)");
        List<Smiley> smileys = message.getSmileys();
        assertThat(smileys.isEmpty(), is(true));
    }

    @Test
    public void testMultipleSmileys() throws Exception {
        Message message = parser.parse("(custom).(success) @Rick aaa,(yeah).");
        List<Smiley> smileys = message.getSmileys();
        assertThat(smileys.size(), is(3));
        assertThat(smileys, equalTo(Arrays.asList(
                new Smiley("custom", ""),
                new Smiley("success", ""),
                new Smiley("yeah", "")
        )));
        List<Mention> mentions = message.getMentions();
        assertThat(mentions.size(), is(1));
        assertThat(mentions.contains(new Mention("Rick")), is(true));
    }

    @Test
    public void testLink() throws Exception {
        String messageWithUrl = "http://mail.ru";
        testSingleLink(messageWithUrl);
    }

    private void testSingleLink(String msg, String link) {
        Message message = parser.parse(msg);
        List<Link> links = message.getLinks();
        assertThat(links.size(), is(1));
        assertThat(links.contains(new Link(link, "")), is(true));
    }

    private void testSingleLink(String messageWithUrl) {
        testSingleLink(messageWithUrl, messageWithUrl);
    }

    @Test
    public void testWrongLink() throws Exception {
        Message message = parser.parse("hhttp://mail.ru");
        assertThat(message.getLinks().isEmpty(), is(true));
    }

    @Test
    public void testHttpsLinks() throws Exception {
        testSingleLink("https://mail.ru");
    }

    @Test
    public void testLinkWithOtherWords() throws Exception {
        testSingleLink("asdf,https://mail.ru sdf", "https://mail.ru");
    }

    @Test
    public void testWords() throws Exception {
        Message message = parser.parse("asd @Alex dd.a.w, fff");
        List<Word> words = message.getWords();
        assertThat(words.size(), is(5));
        assertThat(words, equalTo(Arrays.asList(
                new Word("asd"),
                new Word("dd"),
                new Word("a"),
                new Word("w"),
                new Word("fff")
        )));
    }

    @Test
    public void testDelimiters() throws Exception {
        Message message = parser.parse("asd @Alex dd.a.w, fff");
        List<Delimiter> words = message.getDelimiters();
        assertThat(words.size(), is(5));
        assertThat(words, equalTo(Arrays.asList(
                new Delimiter(" "),
                new Delimiter(" "),
                new Delimiter("."),
                new Delimiter("."),
                new Delimiter(", ")
        )));
    }

    @Test
    public void testWholeStringEquals() throws Exception {
        final String msg = ", @alex, hey check this http://mail.ru (custom)";
        Message message = parser.parse(msg);
        assertThat(message.toString(), is(msg));
        assertThat(message.getAll().size(), is(12));
        assertThat(message.getAll(), equalTo(Arrays.asList(
                new Delimiter(", "),
                new Mention("alex"),
                new Delimiter(", "),
                new Word("hey"),
                new Delimiter(" "),
                new Word("check"),
                new Delimiter(" "),
                new Word("this"),
                new Delimiter(" "),
                new Link("http://mail.ru", ""),
                new Delimiter(" "),
                new Smiley("custom", "")
        )));
    }

    @Test
    public void testFtpDontResolvedAsLink() throws Exception {
        final String msg = "hey, @Alex check this link ftp://a.b.c";
        Message message = parser.parse(msg);
        assertThat(message.getLinks().isEmpty(), is(true));
        assertThat(message.getWords(), hasItems(
                new Word("ftp://a"),
                new Word("b"),
                new Word("c")
        ));
    }

    @Test
    public void testUnicode() throws Exception {
        final String msg = "тестовая строка";
        Message message = parser.parse(msg);
        assertThat(message.getWords(), hasItems(
                new Word("тестовая"),
                new Word("строка")
        ));
    }

    @Test
    public void testSpecialCharactersInMention() throws Exception {
        final String msg = "@Alex%f";
        Message message = parser.parse(msg);
        assertThat(message.getWords().size(), is(1));
        assertThat(message.getMentions().isEmpty(), is(true));
        assertThat(message.getWords().get(0), is(new Word("@Alex%f")));
    }

    @Test
    public void testSpecialCharactersInSmiley() throws Exception {
        final String msg = "@Alex%f ($)";
        Message message = parser.parse(msg);
        assertThat(message.getSmileys().size(), is(0));
        assertThat(message.getWords(), is(Arrays.asList(
                new Word("@Alex%f"),
                new Word("($)")
        )));
    }
}
